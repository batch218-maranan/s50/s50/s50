import "./App.css";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import { Container } from "react-bootstrap";
import Register from "./pages/Register";
import Courses from "./pages/Courses";
import Login from "./pages/Login";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";
import CourseView from "./components/CourseView";

export default function App() {
  // State Hook for the user state
  // Global Scope
  // This stores and validate the user information if a user is logged in on the application or not.
  // const [user, setUser] = useState({ email: localStorage.getItem("email") });

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  // used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      // user is logged in
      .then((res) => res.json())
      .then((data) => {
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
          // user is logged out
        } else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);

  return (
    // Common pattern in React.js for a component to return multiple elements.
    <>
      {/* Initializes the dynamic routing */}
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />

          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/courses" element={<Courses />} />
              <Route path="/courses/:courseId" element={<CourseView />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}
