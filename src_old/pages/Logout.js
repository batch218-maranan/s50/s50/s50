import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

export default function Logout() {

    // consume the UserContext object and destructure it to access user state and unsetUser function from context provider.
    const {unsetUser, setUser} = useContext(UserContext);
  // Clears the value of the local storage when we execute Logout()
//  localStorage.clear();

    unsetUser();

    useEffect(() => {
        setUser({id: null});
    })

    // or we can also use
    //  setUser({email: null})
    
  return (
    <Navigate to="/login" />
  ) 
}
