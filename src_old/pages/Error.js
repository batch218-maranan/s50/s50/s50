import Banner from "../components/Banner"


export default function Error(){

  const data = {
    title: "Error 404 | Page Not Found",
    content: "The page does not exist",
    destination: "/",
    label: "Go back",
  }

  return (
    <Banner data={data} />
  )
}
