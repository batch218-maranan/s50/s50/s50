import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
// import { useNavigate } from 'react-router-dom';
import UserContext from "../UserContext";
import Swal from "sweetalert2";


export default function Login() {
  // Allows us to consume the User Context Object and its properties for user validation
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the value of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // hook that returns a function that lets us navigate to components
  // const navigate = useNavigate()

  console.log(email);
  console.log(password);

  // Function to stimulate redirection via form submission
  function loginUser(e) {
    // prevents page redirection via form submission
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
     })
     .then(res => res.json())
     .then(data => {
        console.log(data);
        

        // Condition that saves the login information of the user if the data is not yet defined
        if(typeof data.access !== "undefined") {
            localStorage.setItem('token', data.access);
            retrieveUserDetails(data.access);

            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to Zuitt!"
            })
            } else {
                Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Please, check your login details and try again."
            })
        }   
     }) 

    // set the email if the authenticated user is in the local storage
    // localStorage.setItem("email", email);
    // setUser({ email: localStorage.getItem("email", email) });

    // Clear input fields
    setEmail("");
    setPassword("");
    // navigate('/')

    // alert("Login successful!");
  };


  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        
        // Changes the global "user" state to store the "id" and the "isAdmin" property od the user which will be used for validation accross the whole application
        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        })
    })
  };

  useEffect(() => {
    if (email !== "" && password !== "" && password.length >= 8) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
    // dependencies - listen to changes made and will execute if condition is made. [] = will immediately execute the function.
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => loginUser(e)}>
      .
      <Form.Text>
        <h2>Login</h2>
      </Form.Text>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          // two-way binding
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>
      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </Form.Group>
      {isActive ? (
        <Button className="mt-2" variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button
          className="mt-2"
          variant="outline-dark"
          type="submit"
          id="submitBtn"
          disabled
        >
          Submit
        </Button>
      )}
    </Form>
  );
}
