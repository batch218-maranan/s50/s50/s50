import React from "react";

// "React.createContext()" - allows to pass information between components without using props drilling
const UserContext = React.createContext();

// "provider" - allows other to consumer/use the context object and supply necessary information need to  the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;